# More Like Apa Kabar

2019/2020 - Story 6 for PPW

## Introduction

- Name  : Alghifari Taufan Nugroho
- NPM   : 1806146846
- URL   : [StoryEnem](https://ppawe-enem.herokuapp.com/)

## Major Commit Changelog

1. Initial Commit | **Story 6 Start**
    - Starting point
    - Try Unittest

2. Add Test, views, urls, templates
    - Added navbar.html and footer.html
    - Try Unittest for models and forms

3. Add homepage
    - Added home.html

4. Add FuncTest
    - Try Functional Test with Selenium

5. Add Models, Forms, fix views | **Story 6 End**
    - Added Status Models and Form
    - Fix views to support form and models

6. Add new app storypitu | **Story 7 Start**
    - Added new app
    - Configure urls to new app
    - Try Unittest

7. Add FuncTest
    - Try Functional Test for Accordion

8. Add content and js | **Story 7 End**
    - Added base.js and profile.js
    - Fixing content assert in Functional Test
    - Add storypitu on coverage

9. Add new app storywolu | **Story 8 Start**
    - Try WoluUnitTest
    - Fix mistype app name

10. Fix books.js | **Story 8 End**
    - Finish Functional Test for /books/
    - Fix ajax js
    - Fix ci

11. Add new app storysongo **Story 9 Start**
    - Added new app
    - Configure urls to new app
    - Try Unittest

12. Commit all **Story 9 End**
    - Finishing storysongo

13. Add new app storysepuluh **Story 10 Start**
    - Added new app
    - Try Unittest

14. Add html, test to green
    - nginx.html with jumbotron only

15. Finishing nginx article **Story 10 End**
    - Added 3 static images
