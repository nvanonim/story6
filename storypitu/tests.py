import time

from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from . import views


# Create your tests here.
class PituFuncTest(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(PituFuncTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(PituFuncTest, self).tearDown()

    def test_open_accordion(self):
        self.browser.get(self.live_server_url + '/profile/')
        time.sleep(1)

        self.browser.find_element(By.CSS_SELECTOR, ".col:nth-child(1) > .accordion-head").click()
        time.sleep(2)

        accHead = self.browser.find_element(By.CSS_SELECTOR, ".col:nth-child(1) > .accordion-content > .accordion-head")
        self.assertEqual(accHead.text, "Kuliah")

        accHead.click()
        time.sleep(2)

        accContent = self.browser.find_element(By.CSS_SELECTOR, ".col:nth-child(1) > .accordion-content > .accordion-content")
        self.assertEqual(accContent.text, "di Fasilkom UI ehe.")

    def test_change_theme(self):
        self.browser.get(self.live_server_url + '/profile/')
        time.sleep(1)

        menu = self.browser.find_element_by_class_name('navbar-toggler')
        menu.send_keys(Keys.RETURN)
        time.sleep(2)

        self.browser.find_element_by_id('switch').click()
        time.sleep(1)

        changed = self.browser.find_element_by_css_selector('body')
        class_ = changed.get_attribute('class')
        self.assertEqual(class_, "min-vh-100 lightm")


class PituUnitTest(TestCase):
    def test_profile_status_code(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_no_url(self):
        response = Client().get('/gg/')
        self.assertEqual(response.status_code, 404)

    def test_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_using_func(self):
        response = resolve('/profile/')
        self.assertEqual(response.func, views.profile)
