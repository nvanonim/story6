from django.shortcuts import render, redirect

# Create your views here.
def staticone(request):
    context = {
        'host': request.get_host(),
        'port': request.get_port(),
    }
    return render(request, 'nginx.html', context)
