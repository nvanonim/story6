from django.test import Client, TestCase
from django.urls import resolve
from . import views

# Create your tests here.
class SepuluhUnitTest(TestCase):
    def test_stone_status_code(self):
        response = Client().get('/blog/stone/')
        self.assertEqual(response.status_code, 200)

    def test_no_url(self):
        response = Client().get('/gg/')
        self.assertEqual(response.status_code, 404)

    def test_stone_template(self):
        response = Client().get('/blog/stone/')
        self.assertTemplateUsed(response, 'nginx.html')

    def test_stone_using_func(self):
        response = resolve('/blog/stone/')
        self.assertEqual(response.func, views.staticone)
