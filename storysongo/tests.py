import time

from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from django.contrib.auth.models import User

from . import views

# Create your tests here.
class SongoFuncTest(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(SongoFuncTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(SongoFuncTest, self).tearDown()

    def test_fill_login(self):
        bw = self.browser
        bw.get(self.live_server_url + '/login/')


class SongoUnitTest(TestCase):
    def test_logout_status_code(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_no_url(self):
        response = Client().get('/gg/')
        self.assertEqual(response.status_code, 404)

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_login_using_func(self):
        response = resolve('/login/')
        self.assertEqual(response.func, views.login_view)

    def test_login_logout(self):
        uname = 'user'
        pw = 'resu123'
        user = User.objects.create_user(username=uname, password=pw)

        # Login
        response = Client().post('/login/', {
        'username': uname,
        'password': pw,
        })
        self.assertRedirects(response, '/books/')

        # Logout
        response = Client().get('/logout/')
        self.assertRedirects(response, '/login/?loggedout')

    def test_login_failed(self):
        uname = 'user'
        pw = 'resuser123'
        user = User.objects.create_user(username=uname, password=pw)

        # Login but failed
        response = Client().post('/login/', {
        'username': uname,
        'password': 'resuser234',
        })

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Login')

    def test_register_user(self):
        uname = 'user'
        pw = 'resu123'

        response = Client().post('/register/', {
        'username': uname,
        'password1': 'resu2345',
        'password2': 'resu2345',
        })

        self.assertRedirects(response, '/login/?needlogin')
