from django.apps import AppConfig


class StorysongoConfig(AppConfig):
    name = 'storysongo'
