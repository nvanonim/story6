from django import forms
from storyenem import models

class StatusForm(forms.Form):
    message = forms.CharField(
        label = 'Status',
        required = True,
        max_length = 300,
        widget = forms.Textarea(attrs = {'class' : 'form-control form-control-sm', 'style' : 'height: 100px;', 'placeholder' : 'Halo, apa kabar?'})
     )

    class Meta:
        model = models.Status
        fields = ('message',)
