from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from . import views, models
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class FunctionalTestEnem(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(FunctionalTestEnem, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTestEnem, self).tearDown()

    def test_sel_fill_form(self):
        self.browser.get(self.live_server_url)
        time.sleep(1)

        msg = self.browser.find_element_by_id('id_message')
        msg.send_keys("Coba Coba")
        
        submit = self.browser.find_element_by_name('Submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        self.assertIn("Coba Coba", self.browser.page_source)
  

class UnitTestEnem(TestCase):
    def test_home_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_no_url(self):
        response = Client().get('/gg/')
        self.assertEqual(response.status_code, 404)

    def test_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.home)

    def test_model_new_status(self):
        prev = models.Status.objects.all().count()
        models.Status.objects.create(message="Ngantoek")
        current = models.Status.objects.all().count()
        self.assertEqual(prev+1, current)

    def test_data_displayed(self):
        pesan = "ngantoek"
        response = Client().post('/', {'message': pesan})
        resp = Client().get('/')
        self.assertIn("ngantoek", resp.content.decode())

    def test_status_model_str(self):
        status = models.Status.objects.create(message="Ngantoek")
        self.assertEqual("Ngantoek", status.__str__())
