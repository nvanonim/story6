from django.shortcuts import render, redirect
from .models import *
from .forms import *

# Create your views here.
def home(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            new_status = Status(
                message = form.data['message']
                )
            new_status.save()
            return redirect('/')
    else:
        form = StatusForm()

    context = {
        'status' : Status.objects.all().order_by('time').reverse().values(),
        'form' : form
    }
    return render(request, 'home.html', context)
