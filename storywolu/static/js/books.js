$(document).ready(function(){
    booksFind("Python");
    $("#search").change(function(){
        booksFind(document.getElementById("search").value);
    });
});

function booksFind(param) {
    // Ambil JSON dari googleapis
    $.ajax({
        url: `https://www.googleapis.com/books/v1/volumes?q=${param}`,
        success: function(output) {
    // $.getJSON(`https://www.googleapis.com/books/v1/volumes?q=${param}`, function(output) {
        let number = 1;
        document.getElementById('output').innerHTML =
        `
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th scope="col" class="mx-auto text-center">Cover</th>
                <th scope="col" class="text-center">Title</th>
                <th scope="col" class="text-center">Authors</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        `;

        output.items.forEach(value => {
            let out = document.createElement("tr");

            // Number
            let num = document.createElement("td");
            num.innerText = number;
            out.appendChild(num);

            // Cover
            let coverOut = document.createElement("td");
            let image = document.createElement("IMG");
            image.alt = value.volumeInfo.title;
            image.src = value.volumeInfo.imageLinks.smallThumbnail;
            coverOut.appendChild(image);
            out.appendChild(coverOut);

            // Title
            let titleOut = document.createElement("td");
            titleOut.innerText = value.volumeInfo.title;
            titleOut.setAttribute('class', 'align-middle');
            out.appendChild(titleOut);

            // Authors
            let authorOut = document.createElement("td");
            authorOut.innerText = value.volumeInfo.authors;
            authorOut.setAttribute('class', 'align-middle');
            out.appendChild(authorOut);

            document.getElementsByTagName("tbody")[0].appendChild(out);
            ++number;
        });
        }
    })
}
