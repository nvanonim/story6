import time

from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from . import views

# Create your tests here.
class PituFuncTest(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(PituFuncTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(PituFuncTest, self).tearDown()

    def test_find_book(self):
        self.browser.get(self.live_server_url + '/books/')
        time.sleep(1)

        search = self.browser.find_element_by_id('search')
        search.send_keys('Java')
        search.send_keys(Keys.RETURN)
        time.sleep(3)

        output = self.browser.find_element_by_css_selector('table')
        getOutput = self.browser.execute_script('return arguments[0].innerText', output)
        self.assertIn('Java', getOutput)

class WoluUnitTest(TestCase):
    def test_profile_status_code(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_no_url(self):
        response = Client().get('/gg/')
        self.assertEqual(response.status_code, 404)

    def test_profile_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_profile_using_func(self):
        response = resolve('/books/')
        self.assertEqual(response.func, views.books)
