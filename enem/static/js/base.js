$("body").addClass(localStorage.getItem("theme"));

$(document).ready(function() {
   $("#switch").click(function() {
       if(localStorage.getItem("theme") === "lightm") {
           localStorage.setItem("theme", "");
           $("body").removeClass("lightm");
       }
       else {
           localStorage.setItem("theme", "lightm")
           $("body").addClass("lightm");
       }
   })
});
